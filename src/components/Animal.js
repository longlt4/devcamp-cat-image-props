import { Component } from "react";
import catImg from "../assets/images/cat.jpg"

class Animal extends Component {
    render() {
        let kind = this.props;
        console.log(kind)
        return (
            <div>
                {!kind ? <p>meow not found</p> :
                <img src = {catImg} alt="cat"></img>}
            </div>  
        )
    }
}
export default Animal;